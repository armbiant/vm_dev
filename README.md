# vm-dev

Module that downloads useful development modules in your project.
No modules are eabled by default. Just enable what you want to use.

Packages:

- https://www.drupal.org/project/coder
- https://www.drupal.org/project/devel
- https://www.drupal.org/project/examples
- https://www.drupal.org/project/potx
- https://www.drupal.org/project/reroute_email
- https://www.drupal.org/project/responsive_preview
- https://www.drupal.org/project/stage_file_proxy
- https://www.drupal.org/project/config_devel
- https://www.drupal.org/project/view_modes_display
- https://www.drupal.org/project/twig_debugger

INSTALL
-------

1. Install using composer
```composer require --dev vanmeeuwen/vm-dev```

2. Add the following to your local.settings.php

```
/**
 * Exclude modules from configuration synchronization.
 *
 * Example:
 * @code
 *   $exclude_project_modules = ['devel', 'stage_file_proxy'];
 * @endcode
 */

// Project modules to be excluded.
$exclude_project_modules = [];

// Development modules from vm-dev to be excluded.
include('../web/modules/contrib/vm-dev/vm_dev.exclude');

// Merge arrays vm development modules and project modules.
/** @var array $exclude_development_modules */
$settings['config_exclude_modules'] = array_merge($exclude_project_modules, $exclude_development_modules);
```
